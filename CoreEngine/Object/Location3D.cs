﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace VoxMod.CoreEngine.Object {
    /// <summary>
    /// The Location3D class is used to provide a cartesian coordinate system
    /// for objects. It can be serialized to a file, and cannot be inherited
    /// from.
    /// </summary>
    [Serializable()]
    public sealed class Location3D : ISerializable {
        #region Constructors
        /// <summary>
        /// Creates a Location3D object at the origin.
        /// </summary>
        public Location3D() : this(0.0, 0.0, 0.0) { }

        /// <summary>
        /// Creates a Location3D object from a single value.
        /// </summary>
        /// <param name="val">The X, Y, and Z coordinate.</param>
        public Location3D(double val) : this(val, val, val) { }

        /// <summary>
        /// Creates a Location3D object from a set of X, Y, and Z coordinates.
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        /// <param name="z">The Z coordinate.</param>
        public Location3D(double x, double y, double z) {
            _X = x;
            _Y = y;
            _Z = z;
        }
        /// <summary>
        /// Constructs a Location3D object from serialized data.
        /// </summary>
        /// <param name="info">The SerializationInfo to use.</param>
        /// <param name="context">The StreamingContext to use.</param>
        public Location3D(SerializationInfo info, StreamingContext context) {
            _X = info.GetDouble("_X_COORD");
            _Y = info.GetDouble("_Y_COORD");
            _Z = info.GetDouble("_Z_COORD");
        }
        #endregion

        #region Serialization
        /// <summary>
        /// Gathers the Location3D object's data for serialization.
        /// </summary>
        /// <param name="info">The SerializationInfo to use.</param>
        /// <param name="context">The StreamingContext to use.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("_X_COORD", _X);
            info.AddValue("_Y_COORD", _Y);
            info.AddValue("_Z_COORD", _Z);
        }

        /// <summary>
        /// Stores a Location3D object in a file.
        /// </summary>
        /// <param name="fileName">The file to store the Location3D object in.</param>
        public void Serialize(string fileName) {
            Location3D.Serialize(fileName, this);
        }

        /// <summary>
        /// Stores a Location3D object in a file.
        /// </summary>
        /// <param name="fileName">The file to store the Location3D object in.</param>
        /// <param name="location">The Location3D object to store.</param>
        public static void Serialize(string fileName, Location3D location) {
            Stream fileStream = File.Open(fileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            Console.WriteLine("Serializing 'Location3D' to '" + fileName + "'.");
            binaryFormatter.Serialize(fileStream, location);
            fileStream.Close();
        }

        /// <summary>
        /// Creates a Location3D object from a file.
        /// </summary>
        /// <param name="fileName">The file to load the Location3D object from.</param>
        /// <returns>The reconstructed Location3D object.</returns>
        public static Location3D Deserialize(string fileName) {
            Stream fileStream = File.Open(fileName, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            Console.WriteLine("Deserializing 'Location3D' from '" + fileName + "'.");
            Location3D retVal = (Location3D)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();

            return retVal;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the X coordinate.
        /// </summary>
        public double X {
            get { return _X; }
            set { _X = value; }
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        public double Y {
            get { return _Y; }
            set { _Y = value; }
        }

        /// <summary>
        /// Gets or sets the Z coordinate.
        /// </summary>
        public double Z {
            get { return _Z; }
            set { _Z = value; }
        }

        /// <summary>
        /// Gets the whole location as IEnumerable set of doubles.
        /// </summary>
        public IEnumerable<double> Values {
            get {
                yield return _X;
                yield return _Y;
                yield return _Z;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Moves the location by a certain amount.
        /// </summary>
        /// <param name="x">Amount to move along the X axis.</param>
        /// <param name="y">Amount to move along the Y axis.</param>
        /// <param name="z">Amount to move along the Z axis.</param>
        public void Shift(double x, double y, double z) {
            _X += x;
            _Y += y;
            _Z += z;
        }
        #endregion

        #region Private Fields
        /// <summary>
        /// The X coordinate.
        /// </summary>
        private double _X;

        /// <summary>
        /// The Y coordinate.
        /// </summary>
        private double _Y;

        /// <summary>
        /// The Z coordinate.
        /// </summary>
        private double _Z;
        #endregion
    }
}
